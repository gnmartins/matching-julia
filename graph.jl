type Graph
    n :: Int64
    m :: Int64 

    adjacent :: Vector{Vector{Int64}}

    Graph(n :: Int64) = new(n, 0, [Vector{Int64}() for i = 1:n])
end

function add_edge!(g :: Graph, u :: Int64, v :: Int64)
    (u > g.n || v > g.n) && return
    push!(g.adjacent[u], v)
    push!(g.adjacent[v], u)
    g.m += 1
    return g
end

edgecount(g :: Graph) = g.m