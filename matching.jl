#!/usr/bin/env julia

include("graph.jl")
include("io.jl")

println("[reading instance...]")
@time g = read_dimacs_matching()

#===================================================#

# TODO: encapsulate these variables

const NULL_VERTEX = g.n+1
const INF = typemax(Int64)

# Nodes = U + V
# left side = U
# right side = V
size_U = convert(Int64, g.n/2)

# mate[u] = v ⟹ (u, v) ∈ M
mate = fill(NULL_VERTEX, g.n+1)

# this serves the same purpose as the hungarian tree
# consider u1, u2 ∈ U and v ∈ V:
# if distance[u2] = distance[u1] + 1 then 
# exists an alternating path (u1 ⇒ v ⇒ u2)
distance = fill(0, g.n+1)

#===================================================#

"""
Performs a breadth first search starting on free vertex on the left side
of the graph to find augmenting alternating paths.

If there is an augmenting alternating path, this function returns true.

The augmenting alternating paths can be found using the `distance` global array.
"""
function bfs(g :: Graph)
    queue = Vector{Int64}()
    # starting queue with unmatched vertices
    for i = 1 : size_U
        if mate[i] == NULL_VERTEX
            distance[i] = 0
            push!(queue, i)
        else
            distance[i] = INF
        end
    end

    distance[NULL_VERTEX] = INF

    while length(queue) > 0
        u = shift!(queue)
        if u != NULL_VERTEX
            # traversing to right side 
            for v in g.adjacent[u]
                # traversing to left side (mate of v)
                # u ∈ U ^ distance[u] = INF ⟹ u is matched
                if distance[mate[v]] == INF
                    distance[mate[v]] = distance[u] + 1
                    push!(queue, mate[v])
                end
            end
        end
    end
    # if distance to the dummy vertex is different from INF,
    # there is a path u ⇒ v ⇒ dummy (i.e. an augmenting alternating path)
    return distance[NULL_VERTEX] != INF
end


"""
Performs a depth first search through augmenting alternating paths
found in the `distance` array, starting on the node `u`. 
This function set the array `mate` based on the traversed path, and returns
true if a match has been found.

The alternating augmenting path is found by recursively traversing the `distance` 
array following the mates of adjacent vertices of `u`. 
`distance[mate[v]] = distance[u] + 1` means that there is an augmenting alternating
path `u ⇒ v ⇒ mate[v]` 

If the dfs recursion reaches the `NULL_VERTEX`, the path has ended. So the recursion
stack can be popped arranging the path's matches.
"""
function dfs(g :: Graph, u :: Int64)
    if u != NULL_VERTEX
        for v in g.adjacent[u]
            if distance[mate[v]] == distance[u] + 1
                if dfs(g, mate[v])
                    mate[u] = v
                    mate[v] = u
                    return true
                end
            end
        end
        distance[u] = INF
        return false
    end
    return true
end

#===================================================#

matches = 0
phases = 0
dfs_calls = 0
println("[starting hopcroft-karp...]")
@time while bfs(g)
    for i = 1 : size_U
        dfs_calls += 1
        if mate[i] == NULL_VERTEX && dfs(g, i)
            matches += 1
        end
    end
    phases += 1
end

println()
println(matches)