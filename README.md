# matching-julia
Maximum cardinality matching solver using [Julia](https://julialang.org/) v0.5.1.

## Implementation
This solver implements the Hopcroft-Karp algorithm. It requires that the input bipartite graph follows the [DIMACS](http://dimacs.rutgers.edu/) matching format.

## Usage
`./matching < input-graph`
