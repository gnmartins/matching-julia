function read_dimacs_matching()
    n, m = 0, 0
    while true
        line = split(readline(STDIN))
        length(line) == 0 && continue
        if line[1] == "p"
            if line[2] == "edge"
                n, m = parse(Int64, line[3]), parse(Int64, line[4])
                break
            end
        end
    end
    
    g = Graph(n)
    
    while edgecount(g) < m
        line = split(readline(STDIN))
        if line[1] == "e"
            add_edge!(g, parse(Int64, line[2]), parse(Int64, line[3]))
        end
    end
    
    return g
end
